(define-module (base packages))

(use-modules (gnu)
             (guix packages)
             (guix utils)
             (guix download)
             (guix git-download)
             ((guix licenses) #:prefix license:)
             (ice-9 match)
             (srfi srfi-1))
             
(use-modules (gnu packages gnome))

(define-public dm/gnome-keyring
  (package
   (inherit gnome-keyring)
   (name "gnome-keyring-sans-ssh-agent")
   (arguments
    (substitute-keyword-arguments
     (package-arguments gnome-keyring)
     ((#:configure-flags flags ''())
      #~(cons "--disable-ssh-agent" #$flags))))))

(define-public dm/gnome
  (package
   (inherit gnome)
   (name "gnome-sans-ssh-agent")
   (propagated-inputs
    (map (match-lambda
           ((name package)
            (if (equal? name "gnome-keyring")
                (list name dm/gnome-keyring)
                (list name package))))
         (package-propagated-inputs gnome)))))             
