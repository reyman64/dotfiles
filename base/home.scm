(define-module (base home)
  #:export (home-module
            home-module?
            home-module-packages
            home-module-services))
            
(use-modules (gnu)
             (gnu services)
             (gnu home)
             (gnu home services)

             (base packages)

             (guix packages)
             (guix download)
             (guix git-download)
             (guix build-system cmake)
             ((guix licenses) #:prefix license:)

             (srfi srfi-1)
             (srfi srfi-9))
             
(define-record-type <home-module>
  (home-module packages services)
  home-module?
  (packages home-module-packages)
  (services home-module-services))           

(define-public (package-module packages)
  (home-module packages '()))

(define-public (service-module services)
  (home-module '() services))

(define-public (append-home-modules modules)
  (home-module
   (reduce append '() (map home-module-packages modules))
   (reduce append '() (map home-module-services modules))))

(define-public (machine-service machine)
  (simple-service 'dotfile-machine-name-environment-service
                  home-environment-variables-service-type
                  `(("DOTFILES_MACHINE" . ,machine))))

(define-public (home-profile-service script)
  "Creates a service that sources a home profile.d script provided by a package."
  (simple-service 'profile.d-service
                  home-shell-profile-service-type
                  `(,(plain-file
                      (string-append script "-profile")
                      (string-append "source ~/.guix-home/profile/etc/profile.d/" script ".sh")))))

(define-public (system-profile-service script)
  "Creates a service that sources a system profile.d script provided by a package."
  (simple-service 'profile.d-service
                  home-shell-profile-service-type
                  `(,(plain-file
                      (string-append script "-profile")
                      (string-append "source /run/current-system/profile/etc/profile.d/" script ".sh")))))


(use-modules (gnu packages shells)
             (gnu packages shellutils)
             (gnu packages terminals)
             (gnu home services shells))

(define %shell-packages (list
                         zsh-syntax-highlighting
                         zsh-autosuggestions
                         alacritty ;; RIP Gnome terminal
                         direnv))

(define %shell-services
  (list
   (service home-zsh-service-type
            (home-zsh-configuration
             (zshrc (list (local-file "./zshrc")))))

   ;; Add scripts and temporary non-guix binaries to path
   (simple-service 'extra-bin-service
                   home-environment-variables-service-type
                   `(("PATH" . "$PATH:$HOME/.dotfiles/bin:$HOME/.bin")
                     ("GUILE_LOAD_PATH" . "$GUILE_LOAD_PATH:$HOME/.dotfiles")))))

(define-public %shell-module
  (home-module %shell-packages %shell-services))

(define %security-services
  (list
   ;; We need to tell GPG where the pinentry program is
   (simple-service 'gpg-config-service
                   home-files-service-type
                   `((".gnupg/gpg-agent.conf"
                      ,(plain-file
                        "gpg-agent"
                        (string-append "pinentry-program /run/current-system/profile/bin/pinentry-gnome3\n"
                                       "enable-ssh-support\n"
                                       ;; Cache timeout of 3 hours
                                       "default-cache-ttl 10800\n"
                                       "default-cache-ttl-ssh 10800\n"
                                       ;; Maximum cache timeout of 1 day
                                       "maximum-cache-ttl 86400\n"
                                       "maximum-cache-ttl-ssh 86400\n")))

                     ;; Enable auth key
                     (".gnupg/sshcontrol"
                      ,(plain-file
                        "sshcontrol"
                        "8968F324DCDDA92D8F6EF7AF5B316757687B2A93\n"))))

   ;; Use GnuPG agent for SSH
   (simple-service 'gpg-ssh-agent-service
                   home-environment-variables-service-type
                   `(("SSH_AUTH_SOCK" . "$(gpgconf --list-dirs agent-ssh-socket)")))))

(define-public %security-module
  (service-module %security-services))
  

(use-modules (gnu packages admin)
             (gnu packages code)
             (gnu packages rust-apps)
             (gnu packages ncdu)
             (gnu packages shellutils)
             (gnu packages mail)
             (gnu packages terminals)
             (gnu packages compression)
             (gnu packages linux)
             (gnu packages sqlite)
             (gnu packages haskell-xyz))  
  
(define-public %utilities-module
  (package-module (list
                   ;; CLI tools
                   htop
                   ncdu
                   ripgrep
                   alacritty
                   fzf
                   direnv
                   fd
                   unzip
                   solaar
                   strace
                   go-gitlab.com-shackra-goimapnotify
                   ;; General tools
                   pandoc)))

(use-modules (gnu packages llvm)
             (gnu packages python)
             (gnu packages node)
             (gnu packages docker)
             (gnu packages cmake)
             (gnu packages statistics)
             (gnu packages commencement)
             (gnu packages base))
             
(define-public %coding-module
  (home-module
   (list clang-toolchain
         gcc-toolchain
         cmake
         gnu-make
         python
         sqlite
         python
         r
         node-lts
         docker-cli)

   (list
    ;; Add pip to path
    (simple-service 'pip-path-service
                    home-environment-variables-service-type
                    `(("PATH" . "$PATH:$HOME/.local/bin")))

    ;; Set npm config
    (simple-service 'npm-config-service
                    home-files-service-type
                    `((".npmrc" ,(plain-file "npmrc"
                                             "prefix=~/.npm_global\n"))))

    ;; Add npm to path
    (simple-service 'npm-path-service
                    home-environment-variables-service-type
                    `(("PATH" . "$PATH:$HOME/.npm_global/bin"))))))

(use-modules (gnu packages package-management))

(define %nix-services
  (list
   (system-profile-service "nix")
   (simple-service 'nix-unfree-config-service
                   home-files-service-type
                   `((".config/nixpkgs/config.nix" ,(plain-file "nix-unfree-config"
								"{ allowUnfree = true; }"))))

   (simple-service 'nix-env-service
                   home-environment-variables-service-type
                   ;; For desktop files to work we need to not only source the nix
                   ;; env in our interactive shell, but also add the nix path to the
                   ;; global profile
                   `(("PATH" . "$PATH:$HOME/.nix-profile/bin")
                     ("XDG_DATA_DIRS" . "$XDG_DATA_DIRS:$HOME/.nix-profile/share")
                     ("XDG_CONFIG_DIRS" . "$XDG_CONFIG_DIRS:$HOME/.nix-profile/etc/xdg")))))

(define-public %nix-module
  (home-module (list nix) %nix-services))

(use-modules (nongnu packages mozilla)
             (gnu packages maths)
             (flat packages emacs)
             (gnu packages libreoffice)
	     (guix-science packages rstudio)
             (gnu packages inkscape)
             (gnu packages gimp)
             (gnu packages video)
             (gnu packages audio)
             (gnu packages qt)
             (gnu packages pdf)
             (gnu packages virtualization))             
	
(define %applications-packages
  (list
   rstudio
   emacs-native-comp
   firefox
   libreoffice
   inkscape
   gimp
   vlc
   handbrake
   ffmpeg
   audacity
   xournalpp
   ;; QT apps require this module to use icons, and some apps
   ;; incorrectly don't have it as a dependency.
   qtsvg))

(define %applications-services
  (list
   (system-profile-service "flatpak")))

(define-public %applications-module
  (home-module %applications-packages %applications-services))

(use-modules (gnu packages fonts)
             (gnu packages gnome)
             (gnu packages fontutils)
             (gnu packages gnome-xyz))

(define-public %fonts-module
  (home-module (list
                fontconfig
                font-fira-mono
                font-fira-sans
                font-hack
                font-lato
                font-google-noto
                font-google-roboto
                gnome-icon-theme
                adwaita-icon-theme
                hicolor-icon-theme
                arc-icon-theme
                font-awesome)
               '()))


(define-public %full-module
  (append-home-modules (list
                        %security-module
                        %utilities-module
                        %coding-module
                        %nix-module
                        %applications-module
                        %fonts-module)))

(define-public %dm/home-packages (home-module-packages %full-module))
(define-public %dm/home-services (home-module-services %full-module))


