(use-modules (base home)
             (gnu home))

(home-environment
 (packages %dm/home-packages)
 (services (cons*
            (machine-service "Suhayl")
            %dm/home-services)))
